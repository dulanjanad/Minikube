### Minikube

I am using below environment to setup Minikube Kubernetes Cluster.

CentOS Linux release 7.9.2009
Docker version 20.10.20

#### Build a docker image with the sample code

Before checking on minikube things, below steps will show how to create docker image for Incremental and Decremental counter and push it to DocketHub.

1). Pull code from repository and change to Docker directory.
    
    git pull 
    cd Docker
    
2). Build the docker image. 

    docker build -t webapp .

3). Tag the docker image as per the docker hub repository(public) that you are using

    ex: docker tag webapp:latest dulanjananiroshann/webapp:latest

4). Login to dockerhub via terminal by providing the credentials.

    docker login 

5). Push Docker image to dockerhub.

    docker push dulanjananiroshann/webapp:latest
    
#### Setup Minikube

1). Install Minikube as per the documentation.

    https://minikube.sigs.k8s.io/docs/start/

2). Install kubectl to interact with the cluster.

    https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

3). Start the minikube cluster.

    minikube start
    
   ![Diagram-1](Diagrams/Minikube.png)
    
4). Check whether control plane and rest of the things are running.

    kubectl get all -A
    
   ![Diagram-2](Diagrams/kubectl-all.png)

5). Deploy secrets to minikube.

    kubectl apply -f secret.yaml
 
6). Deploy webapp to minikube.

    kubectl apply -f webapp-deployment.yaml
    
7). Use mysql-deployment.yaml file to deploy MySQL to minikube cluster.

    kubectl apply -f mysql-deployment.yaml

8). Check whether pods are running.

    kubectl get all,secrets
   
   ![Diagram-3](Diagrams/After-Deploy.png)

9). Get URL from minikube serivce for webapp

    minikube service http-server
    
   ![Diagram-4](Diagrams/minikube-service.png)
   
    browse the URL generated, you will see the website
    
   ![Diagram-5](Diagrams/Increment-Counter.png)

10). Create tables and databses on MySQL pod created

    kubectl exec -it <mysql-pod-name> -- /bin/bash
    CREATE DATABASE YAY;
    CREATE TABLE table_name (
    column1 datatype,
    column2 datatype,
    column3 datatype
    );
    
    verify database and tables created
    
    show databases;
    use YAY;
    show tables;
   
   ![Diagram-6](Diagrams/MySQL.png)

### I will be using Helm charts to deploy Prometheues and Grafana to Minikube

1). Install Helm3.

    Follow below official guide - https://helm.sh/docs/intro/install/

2). Install Promehtues with Helm.

    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm install prometheus prometheus-community/prometheus

3). Expose NodePort service for prometheus.

    kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-nodeport

4). Get URL for Prometheus.

    minikube service prometheus-server-nodeport

5). Install Grafana with Helm.

    helm repo add grafana https://grafana.github.io/helm-charts
    helm install grafana grafana/grafana

6). Expose NodePort service for Grafana.

    kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-nodeport
    
7). Get URL for Grafana.

    minikube service grafana-nodeport
